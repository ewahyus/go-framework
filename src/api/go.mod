module github.com/opam22/form

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-sql-driver/mysql v1.4.1
	github.com/lib/pq v1.0.0
)
